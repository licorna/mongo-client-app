#!/usr/bin/env python3

"""
Continuously writes document to a MongoDB Database.
"""

from pymongo import MongoClient
from datetime import datetime
import os
import sys
import time

if __name__ == "__main__":
    uri = os.getenv("MONGODB_HOST")
    if uri is None:
        if len(sys.argv) < 2:
            print("Need to specify MongoDB HOST")
            sys.exit(1)
        else:
            uri = sys.argv[1]

    print("Connecting to mongodb+srv://{}".format(uri))
    client = MongoClient(uri)
    db = client.test_database
    text = "Writing simple document to {}".format(uri)

    while True:
        time.sleep(0.1)
        try:
            ts = datetime.utcnow()
            document = {
                "text": text,
                "timestamp": ts
            }

            result = db.posts.insert_one(document)
            print("Document inserted - {} at {}".format(result.inserted_id, ts))
        except Exception as e:
            print("Failed to insert document")
