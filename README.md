# mongo-client-app

This small app will connect to a mongodb instance running in
whatever is passed as `MONGODB_HOST` environment variable and
will keep inserting documents with a sleep of 0.1 seconds
inbetween. Can be run locally or with `kubectl` like:

```bash
kubectl run mongo-client-app \
    --image=quay.io/rodrigo_valin/mongo-client-app:latest \
    --env="MONGODB_HOST=my_mongodb_host"
```

This will run a Pod forever inserting data into `my_mongodb_host`.
It is expected that the mongod service is listening in default
27017 port.

# Is this image built somewhere?

This image is built automatically when pushing to master repo. 
The location is: quay.io/rodrigo_valin/mongo-client-app