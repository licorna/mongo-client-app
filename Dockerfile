FROM python:3-alpine

ADD run.py /
RUN pip install pymongo

ENTRYPOINT [ "python", "run.py" ]
CMD []
